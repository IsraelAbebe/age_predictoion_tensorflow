import tensorflow as tf
from preprocess import get_testing_data, get_training_data

# Parameters
learning_rate = 0.001
training_iters = 10000
batch_size = 50
display_step = 20

# Network  parameters
n_input = 68 * 2  # 68*2
n_classes = 8
dropout = 0.75

# Weights and biases
weights = {
    # 'w_conv1': tf.Variable(tf.random_normal([5, 5, 1, 32])),
    # 'w_conv2': tf.Variable(tf.random_normal([5, 5, 32, 64])),
    # 'w_conv3': tf.Variable(tf.random_normal([5, 5, 64, 128])),
    # 'w_conv4': tf.Variable(tf.random_normal([5, 5, 128, 256])),
    'w_fc1': tf.Variable(tf.random_normal([136, 1024])),
    'w_fc2': tf.Variable(tf.random_normal([1024, 2048])),
    'w_fc3': tf.Variable(tf.random_normal([2048, 4096])),
    'w_fc4': tf.Variable(tf.random_normal([4096, 8192])),
    'W_out': tf.Variable(tf.random_normal([8192, n_classes]))
}

biases = {
    # 'b_conv1': tf.Variable(tf.random_normal([32])),
    # 'b_conv2': tf.Variable(tf.random_normal([64])),
    # 'b_conv3': tf.Variable(tf.random_normal([128])),
    # 'b_conv4': tf.Variable(tf.random_normal([256])),
    'b_fc1': tf.Variable(tf.random_normal([1024])),
    'b_fc2': tf.Variable(tf.random_normal([2048])),
    'b_fc3': tf.Variable(tf.random_normal([4096])),
    'b_fc4': tf.Variable(tf.random_normal([8192])),
    'b_out': tf.Variable(tf.random_normal([n_classes]))
}

# tf Graph inputs
# tf Graph inputs
x = tf.placeholder(tf.float32, [None, n_input], name="inputs")
y = tf.placeholder(tf.float32, [None, n_classes], name="output")
keep_prob = tf.placeholder(dtype=tf.float32)

train_data = get_training_data('data/data_train_land.csv')
test_data = get_testing_data('data/data_test_land.csv')

train_data_images = train_data[:, :n_input]
train_data_labels = train_data[:, n_input:]

test_data_images = test_data[:, :n_input]
test_data_labels = test_data[:, n_input:]


def conv2d(x, W, b, strides=1):
    '''
    :param x:
    :param W:
    :param b:
    :param strides:
    :return:
    '''
    x = tf.nn.conv2d(x, filter=W, strides=[1, strides, strides, 1], padding="SAME")
    x = tf.nn.bias_add(x, b)
    return tf.nn.relu(x)


def maxpool2d(x, k=2):
    '''
    :param x: input
    :param k: kernel size
    :return:
    '''
    return tf.nn.max_pool(x, ksize=[1, k, k, 1], strides=[1, k, k, 1], padding="SAME")


def conv_net(x, weights, biases, dropout):
    '''
    :param x:
    :param weights:
    :param biases:
    :param dropout:
    :return:
    '''
    x = tf.reshape(x, shape=[-1, 68, 2, 1])

    # fully connected one
    fc_1 = tf.reshape(x, [-1, weights['w_fc1'].get_shape().as_list()[0]])
    fc_1 = tf.add(tf.matmul(fc_1, weights['w_fc1']), biases['b_fc1'])
    fc_1 = tf.nn.relu(fc_1)

    # fully connected two
    fc_2 = tf.reshape(fc_1, [-1, weights['w_fc2'].get_shape().as_list()[0]])
    fc_2 = tf.add(tf.matmul(fc_2, weights['w_fc2']), biases['b_fc2'])
    fc_2 = tf.nn.relu(fc_2)

    # fully connected three
    fc_3 = tf.reshape(fc_2, [-1, weights['w_fc3'].get_shape().as_list()[0]])
    fc_3 = tf.add(tf.matmul(fc_3, weights['w_fc3']), biases['b_fc3'])
    fc_3 = tf.nn.relu(fc_3)

    # fully connected four
    fc_4 = tf.reshape(fc_3, [-1, weights['w_fc4'].get_shape().as_list()[0]])
    fc_4 = tf.add(tf.matmul(fc_4, weights['w_fc4']), biases['b_fc4'])
    fc_4 = tf.nn.relu(fc_4)

    # Apply dropout
    fc_out = tf.nn.dropout(fc_4, dropout)
    out = tf.add(tf.matmul(fc_out, weights['W_out']), biases['b_out'])

    return out


# construct a model
prediction = conv_net(x, weights, biases, keep_prob)

# define loss and optimizer
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=prediction, labels=y))
optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost)

# Evaluate model
correct_prediction = tf.equal(tf.argmax(prediction, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, dtype=tf.float32))

# initialize vars
init = tf.global_variables_initializer()

saver = tf.train.Saver()
# launch the graph
with tf.Session() as sess:
    sess.run(init)
    step = 1
    while step * batch_size < training_iters:
        batch_x = train_data_images[step * batch_size:(step + 1) * batch_size]
        batch_y = train_data_labels[step * batch_size:(step + 1) * batch_size]
        sess.run(optimizer, feed_dict={x: batch_x, y: batch_y, keep_prob: 1.})
        step += 1
        print step

    print("optimization Finished!")
    print("Testing Accuracy:",
          sess.run(accuracy, feed_dict={x: test_data_images, y: test_data_labels, keep_prob: 1.}))
    saver.save(sess, 'model/my_test_model')
