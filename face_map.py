from imutils import face_utils
import numpy as np
import imutils
import dlib
import cv2


def give_facemap(image):
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')

    image = cv2.imread(image)
    image = imutils.resize(image, width=256)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    rects = detector(gray, 1)

    a = []
    for (i, rect) in enumerate(rects):
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)

        (x, y, w, h) = face_utils.rect_to_bb(rect)
        for (x, y) in shape:
            a.append([x, y])

        break

    a = np.array(a)
    if len(a) == 0:
        return None
    else:
        return a


def map_to_bin(map_array):
    MAP_SIZE = 68
    IMAGE_WIDTH = 256
    IMAGE_HEIGHT = 256

    image_array = np.zeros([IMAGE_WIDTH, IMAGE_HEIGHT], dtype=float)

    for i in range(len(map_array)):
            try:
                image_array[map_array[i][0]][map_array[i][1]] = 1.0
            except IndexError:
                continue
    return image_array
