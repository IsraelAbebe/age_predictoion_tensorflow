import tensorflow as tf
import numpy as np

n_input = 68 * 2  # 256 x 256
n_classes = 8

x = tf.placeholder(tf.float32, [None, n_input], name="inputs")
y = tf.placeholder(tf.float32, [None, n_classes], name="output")

weights = {
    # 'w_conv1': tf.Variable(tf.random_normal([5, 5, 1, 32])),
    # 'w_conv2': tf.Variable(tf.random_normal([5, 5, 32, 64])),
    # 'w_conv3': tf.Variable(tf.random_normal([5, 5, 64, 128])),
    # 'w_conv4': tf.Variable(tf.random_normal([5, 5, 128, 256])),
    'w_fc1': tf.Variable(tf.random_normal([136, 1024])),
    'w_fc2': tf.Variable(tf.random_normal([1024, 2048])),
    'w_fc3': tf.Variable(tf.random_normal([2048, 4096])),
    'w_fc4': tf.Variable(tf.random_normal([4096, 8192])),
    'W_out': tf.Variable(tf.random_normal([8192, n_classes]))
}

biases = {
    # 'b_conv1': tf.Variable(tf.random_normal([32])),
    # 'b_conv2': tf.Variable(tf.random_normal([64])),
    # 'b_conv3': tf.Variable(tf.random_normal([128])),
    # 'b_conv4': tf.Variable(tf.random_normal([256])),
    'b_fc1': tf.Variable(tf.random_normal([1024])),
    'b_fc2': tf.Variable(tf.random_normal([2048])),
    'b_fc3': tf.Variable(tf.random_normal([4096])),
    'b_fc4': tf.Variable(tf.random_normal([8192])),
    'b_out': tf.Variable(tf.random_normal([n_classes]))
}


def predict(X):
    x = tf.reshape(X, shape=[ 68*2])

    # fully connected one
    fc_1 = tf.reshape(x, [-1, weights['w_fc1'].get_shape().as_list()[0]])
    fc_1 = tf.add(tf.matmul(fc_1, weights['w_fc1']), biases['b_fc1'])
    fc_1 = tf.nn.relu(fc_1)

    # fully connected two
    fc_2 = tf.reshape(fc_1, [-1, weights['w_fc2'].get_shape().as_list()[0]])
    fc_2 = tf.add(tf.matmul(fc_2, weights['w_fc2']), biases['b_fc2'])
    fc_2 = tf.nn.relu(fc_2)

    # fully connected three
    fc_3 = tf.reshape(fc_2, [-1, weights['w_fc3'].get_shape().as_list()[0]])
    fc_3 = tf.add(tf.matmul(fc_3, weights['w_fc3']), biases['b_fc3'])
    fc_3 = tf.nn.relu(fc_3)

    # fully connected four
    fc_4 = tf.reshape(fc_3, [-1, weights['w_fc4'].get_shape().as_list()[0]])
    fc_4 = tf.add(tf.matmul(fc_4, weights['w_fc4']), biases['b_fc4'])
    fc_4 = tf.nn.relu(fc_4)

    # Apply dropout
    fc_out = tf.nn.dropout(fc_4, 0.75)
    prediction = tf.add(tf.matmul(fc_out, weights['W_out']), biases['b_out'])

    # saver = tf.train.Saver()
    saver = tf.train.Saver()
    with tf.Session() as sess:
        # restore the model
        saver.restore(sess, tf.train.latest_checkpoint('./model/'))
        P = sess.run(prediction, feed_dict={x: X})

    class_value = P
    class_value = class_value.tolist()
    name = class_value[0].index(max(class_value[0]))
    return name




